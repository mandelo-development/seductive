function xhrRequest(path, data, state) {
	return new Promise(resolve => {
		var text = [],
			language = document.getElementsByTagName('html')[0].getAttribute('lang'),
			i;
		for (i in data) {
			if (data.hasOwnProperty(i)) {
				if (data[i].length > 0 || i == 'page') {
					text.push(i + "=" + encodeURIComponent(data[i]));
				}
			}
		}
		text = text.join("&").split(' ').join('_');
		if (path.includes('http')) {
			var url = path + "?" + text;
		} else {
			var url = '/' + language + '/' + path + "?" + text;
		}
		if (state) {
			window.history.pushState(null, null, "?" + text);
		}
		var xhr = window.XMLHttpRequest ? new XMLHttpRequest : window.ActiveXObject ? new ActiveXObject("Microsoft.XMLHTTP") : (alert("Browser does not support XMLHTTP."), false);
		xhr.onreadystatechange = text;
		xhr.open("GET", url, true);
		xhr.setRequestHeader("Content-Type", "application/json");
		xhr.send(text);
		xhr.onload = function() {
			let result = xhr.response;
			resolve(xhr.response);
		};
	});
}
export {
	xhrRequest
}