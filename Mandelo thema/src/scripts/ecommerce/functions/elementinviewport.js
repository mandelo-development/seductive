function elementInViewport(el, offset) {
	if (el) {
		let top = el.offsetTop;
		let height = el.offsetHeight;
		while (el.offsetParent) {
			el = el.offsetParent;
			top += el.offsetTop;
		}
		return (
			(window.pageYOffset + window.innerHeight) > (height + top - offset)
		);
	}
}

export {
	elementInViewport
}