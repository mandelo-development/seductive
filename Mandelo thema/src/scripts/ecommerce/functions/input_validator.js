function inputValidator(input) {
	if (input.value) {
		if (input.type == "email" && !validateEmail(input.value)) {
			input.classList.add('pc--error');
			return false;
		} else if (input.type == "tel" && validatePhone(input.value)) {
			input.classList.add('pc--error');
			return false;
		} else if ((input.type == "checkbox" || input.type == "radio") && input.required) {
			if (document.querySelector('input[name="' + input.name + '"]:checked')) {
				input.classList.remove('pc--error');
				return true;
			} else {
				input.classList.add('pc--error');
				return false;
			}
		} else {
			input.classList.remove('pc--error');
			return true;
		}
	} else if (input.required) {
		input.classList.add('pc--error');
		return false;
	} else {
		input.classList.remove('pc--error');
		return true;
	}
}

function validateEmail(email) {
	const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(email);
}

function validatePhone(phone) {
	const re = /^[a-zA-Z]+$/;
	return re.test(phone);
}
export {
	inputValidator
}