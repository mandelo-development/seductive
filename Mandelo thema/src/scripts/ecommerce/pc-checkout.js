import {
	devLog
} from './functions/devlog'
import {
	PlateCommerce
} from './functions/PlateCommerce'
import {
	inputValidator
} from './functions/input_validator'
import {
	xhrRequest
} from './functions/request'
let {
	Cart,
	OrderHandler,
	Config,
	Order
} = PlateCommerce
let currentButton;

const cartChanged = new Event('cartChanged');
const totalChanged = new Event('totalChanged');
let oldShipping = {}
let oldBilling = {}



async function pcCheckout() {
	let billingAddress = Cart.getBillingAddress();
	let shippingAddress = Cart.getShippingAddress();
	let splitstreet, splitname, splitstreetShipping;
	if (billingAddress) {
		splitstreet = billingAddress.street.split('__');
		splitname = billingAddress.name.split('__');
	}
	if (shippingAddress) {
		splitstreetShipping = shippingAddress.street.split('__');
	}
	let notes = Cart.getNotes()
	document.querySelectorAll('.pc__checkout').forEach((checkout, i) => {
		insertValue('input[name="email"]', Cart.getEmail())
		if (billingAddress) {
			insertValue('.pc__account-info__name', splitname[0])
			insertValue('.pc__account-info__lastname', splitname[1])
			insertValue('input[name="companyName"]', billingAddress, 'companyName')
			insertValue('input[name="city"]', billingAddress, 'city')
			insertValue('input[name="postalCode"]', billingAddress, 'postalCode')
			insertValue('.pc__account-info__billing-street', splitstreet[0])
			insertValue('.pc__account-info__billing-number', splitstreet[1])
			insertValue('.pc__account-info__billing-addition', splitstreet[2])
			insertValue('input[name="phone"]', billingAddress, 'phone')
		}
		if (shippingAddress) {
			insertValue('input[name="city"]', shippingAddress, 'city')
			insertValue('input[name="postalCode"]', shippingAddress, 'postalCode')
			insertValue('.pc__account-info__shipping-street', splitstreetShipping[0])
			insertValue('.pc__account-info__shipping-number', splitstreetShipping[1])
			insertValue('.pc__account-info__shipping-addition', splitstreetShipping[2])
			insertValue('textarea[name="notes"]', notes)
		}

		function insertValue(selector, value, valueProperty) {
			checkout.querySelectorAll(selector).forEach((item, i) => {
				if (valueProperty) {
					if (value) {
						if (value[valueProperty]) {
							item.value = value[valueProperty]
						}
					}
				} else {
					if (value) {
						item.value = value
					}
				}
			});
		}
	})
	if (Cart.getShippingAddress()) {
		document.querySelectorAll('.pc__account-info__parent--shipping input[name="city"]').forEach((item, i) => {
			item.value = Cart.getShippingAddress().city
		});
		document.querySelectorAll('.pc__account-info__parent--shipping input[name="postalCode"]').forEach((item, i) => {
			item.value = Cart.getShippingAddress().postalCode
		});
	}

	let checkoutValues = {}
	let checkoutFields = document.querySelectorAll('.pc__account-info__field:not(.pc__payment__item__input)');
	checkoutFields.forEach((field, i) => {
		field.addEventListener('change', function() {
			getValues()
			devLog('Current order details', Cart.getOrder());
		})
	});
	let shippingIsBilling = document.querySelector('#billing_is_shipping')
	if (shippingIsBilling) {
		if (Cart.getBillingAddress() || Cart.getShippingAddress()) {
			if (Cart.getBillingAddress().street != Cart.getShippingAddress().street || Cart.getBillingAddress().city != Cart.getShippingAddress().city || Cart.getBillingAddress().postalCode != Cart.getShippingAddress().postalCode) {
				document.querySelectorAll('.pc__account-info__parent--shipping').forEach((billing, i) => {
					billing.style.display = "block";
				});
				shippingIsBilling.checked = false;
			}
		}
		shippingIsBilling.addEventListener('change', function() {
			isShippingBilling()
			updateCartValues()
		})
	}

	function isShippingBilling() {
		if (shippingIsBilling.checked) {
			document.querySelectorAll('.pc__account-info__parent--shipping').forEach((billing, i) => {
				billing.style.display = "none";
				billing.querySelectorAll('input').forEach((input, i) => {
					input.required = false;
				});

				if (Cart.getBillingAddress()) {
					Cart.setShippingAddress(Cart.getBillingAddress(), true)
				}
			});
		} else {
			document.querySelectorAll('.pc__account-info__parent--shipping').forEach((billing, i) => {
				billing.style.display = "block";
				billing.querySelectorAll('input').forEach((input, i) => {
					if (input.dataset.required == "true") {
						input.required = true;
					}
				});
			});
			// getValues()
		}
	}


	// Get values
	function getValues() {
		let checkout = document.querySelector('.pc__checkout')
		let billingValues = checkout.querySelector('.pc__account-info__parent--billing')
		let shippingValues = checkout.querySelector('.pc__account-info__parent--shipping')

		checkoutValues = {
			billingAddress: {
				city: billingValues.querySelector('.pc__account-info__billing-city').value,
				companyName: checkout.querySelector('.pc__account-info__companyName').value,
				name: checkout.querySelector('.pc__account-info__name').value + '__' + checkout.querySelector('.pc__account-info__lastname').value,
				phone: checkout.querySelector('.pc__account-info__phone').value,
				postalCode: billingValues.querySelector('.pc__account-info__billing-zip').value,
				street: billingValues.querySelector('.pc__account-info__billing-street').value + '__' + billingValues.querySelector('.pc__account-info__billing-number').value + '__' + billingValues.querySelector('.pc__account-info__billing-addition').value
			},
			shippingAddress: {
				city: shippingValues.querySelector('.pc__account-info__shipping-city').value,
				companyName: checkout.querySelector('.pc__account-info__companyName').value,
				name: checkout.querySelector('.pc__account-info__name').value + '__' + checkout.querySelector('.pc__account-info__lastname').value,
				phone: checkout.querySelector('.pc__account-info__phone').value,
				postalCode: shippingValues.querySelector('.pc__account-info__shipping-zip').value,
				street: shippingValues.querySelector('.pc__account-info__shipping-street').value + '__' + shippingValues.querySelector('.pc__account-info__shipping-number').value + '__' + shippingValues.querySelector('.pc__account-info__shipping-addition').value
			},
			email: checkout.querySelector('.pc__account-info__email').value,
			redirectUrl: checkout.querySelector('.pc__account-info__redirectUrl').value,
			notes: checkout.querySelector('.pc__account-info__notes').value
		};
		isShippingBilling()
		updateCartValues()
	}

	function updateCartValues() {
		if (checkoutValues.shippingAddress) {
			if (document.querySelector('#billing_is_shipping').checked) {
				Cart.setShippingAddress(checkoutValues.billingAddress, true)
			} else {
				Cart.setShippingAddress(checkoutValues.shippingAddress, true)
			}
		}
		if (checkoutValues.billingAddress) {
			Cart.setBillingAddress(checkoutValues.billingAddress, true)
		}
		if (checkoutValues.email) {
			Cart.setEmail(checkoutValues.email)
		}
		if (checkoutValues.redirectUrl) {
			Cart.setRedirectUrl(checkoutValues.redirectUrl + '?successfull_order=true')
		}
		if (checkoutValues.notes) {
			Cart.setNotes(checkoutValues.notes)
		}
	}



	// Submit checkout to payment
	document.querySelectorAll('.pc__checkout__to_payment_provider').forEach((paymentForm, i) => {
		if (Cart.getItems().length > 0) {
			paymentForm.querySelector('.pc__checkout__to_payment_provider__button').disabled = false;
		} else {
			paymentForm.querySelector('.pc__checkout__to_payment_provider__button').disabled = true;
		}
		paymentForm.addEventListener('submit', function(event) {
			event.preventDefault();

			let validation = true;
			document.querySelectorAll('.pc__checkout input, .pc__checkout textarea, .pc__checkout select').forEach((input, i) => {
				if (validation) {
					validation = inputValidator(input);
				} else {
					inputValidator(input);
				}
			});
			if (submitValidation() && validation) {
				getValues()
				let tempShipping = Cart.getShippingAddress();
				let tempBilling = Cart.getBillingAddress();
				oldShipping = Cart.getShippingAddress();
				oldBilling = Cart.getBillingAddress();
				tempBilling.name = tempBilling.name.replace('__', ' ').replace('__', ' ');
				tempBilling.street = tempBilling.street.replace('__', ' ').replace('__', ' ');
				Cart.setShippingAddress(tempShipping, true);
				Cart.setBillingAddress(tempBilling, true);
				OrderHandler.placeOrderFromCart().then((data) => {
					Cart.setShippingAddress(oldShipping);
					Cart.setBillingAddress(oldBilling);
					let orderSuccesfull = new CustomEvent('orderSuccesfull', {
						"detail": data
					});
					document.dispatchEvent(orderSuccesfull);
					window.location.href = data.data.redirectUrl;
				}).catch((error) => {
					Cart.setShippingAddress(oldShipping);
					Cart.setBillingAddress(oldBilling);
					let orderNotSuccesfull = new CustomEvent('orderNotSuccesfull', {
						"detail": error.response
					});
					document.dispatchEvent(orderNotSuccesfull);
				})
			}
		})
	});
	pcPayment();
}

function pcShipping(firstLoad) {
	let shippingInfo = {}
	let shippings = document.querySelectorAll('.pc__shipping');
	if (shippings[0]) {
		shippings.forEach((shipping, i) => {
			shipping.classList.add('pc__shipping--loading')
		});
		OrderHandler.computeOrderFromCart().then((computation) => {
			shippingInfo['shipping_rates'] = computation.subtotalWithTaxes + '';
			let result;
			async function request() {
				result = await xhrRequest('platecommerce', shippingInfo, false);
			}
			request().then(function() {
				shippings.forEach((shipping, i) => {
					shipping.classList.remove('pc__shipping--loading')
					shipping.innerHTML = result;
					shipping.querySelectorAll('.pc__shipping__item__input').forEach((shipping, i) => {
						if (shipping.value == Cart.getShippingMethodId()) {
							shipping.checked = true;
						}
						shipping.addEventListener('change', function() {
							if (this.checked) {
								Cart.setShippingMethodId(this.value);
								document.dispatchEvent(totalChanged);
							}
						})
					});
				});
			})
		})
	}
}

function pcPayment() {
	document.querySelectorAll('.pc__payment__item__input').forEach((payment, i) => {
		if (payment.value == Cart.getPaymentProvider()) {
			payment.checked = true;
		}
		payment.addEventListener('change', function() {
			Cart.setPaymentProvider(this.value)
			if (this.dataset.payment_method) {
				Cart.setPaymentProviderParameters({
					method: this.dataset.payment_method
				})
			}
			devLog('Current order details', Cart.getOrder());
		})
	});

}

function submitValidation() {
	let allValues = {
		billingAddress: {
			city: Cart.getBillingAddress() ? Cart.getBillingAddress().city : undefined,
			name: Cart.getBillingAddress() ? Cart.getBillingAddress().name.replace('__', '') : undefined,
			postalCode: Cart.getBillingAddress() ? Cart.getBillingAddress().postalCode : undefined,
			street: Cart.getBillingAddress() ? Cart.getBillingAddress().street : undefined
		},
		shippingAddress: {
			city: Cart.getShippingAddress() ? Cart.getShippingAddress().city : undefined,
			name: Cart.getShippingAddress() ? Cart.getShippingAddress().name.replace('__', '') : undefined,
			postalCode: Cart.getShippingAddress() ? Cart.getShippingAddress().postalCode : undefined,
			street: Cart.getShippingAddress() ? Cart.getShippingAddress().street : undefined
		},
		email: Cart.getEmail() ? Cart.getEmail : undefined,
		paymentProvider: Cart.getPaymentProvider(),
		shippingMethodId: Cart.getShippingMethodId()
	}
	let errorValues = [];
	for (var property in allValues) {
		if (allValues.hasOwnProperty(property)) {

			if (allValues[property].city || allValues[property].name || allValues[property].postalCode || allValues[property].street) {
				for (var prop in allValues[property]) {
					if (allValues[property].hasOwnProperty(prop)) {
						let value = allValues[property][prop];
						if (value == "" || value == undefined) {
							errorValues.push({
								parent: property,
								value: prop
							})
						}
					}
				}
			} else {
				let value = allValues[property];
				if (value == "" || value == undefined) {
					errorValues.push({
						value: property
					})
				}
			}
		}
	}
	errorValues.forEach((error, i) => {
		if (error.value == "email") {
			document.querySelector('.pc__account-info__email').classList.add('pc--error')
		} else if (error.value == "paymentProvider") {
			document.querySelectorAll('.pc__payment__item__input').forEach((payment, i) => {
				payment.classList.add('pc--error')
			});
		} else if (error.value == "shippingMethodId") {
			document.querySelectorAll('.pc__shipping__item__input').forEach((method, i) => {
				method.classList.add('pc--error')
			});
		} else if (error.value == "name") {
			document.querySelector('.pc__account-info__name').classList.add('pc--error')
			document.querySelector('.pc__account-info__lastname').classList.add('pc--error')
		} else if (error.value == "city") {
			if (error.parent == "shippingAddress") {
				document.querySelector('.pc__account-info__shipping-city').classList.add('pc--error')
			} else {
				document.querySelector('.pc__account-info__billing-city').classList.add('pc--error')
			}
		} else if (error.value == "postalCode") {
			if (error.parent == "shippingAddress") {
				document.querySelector('.pc__account-info__shipping-zip').classList.add('pc--error')
			} else {
				document.querySelector('.pc__account-info__billing-zip').classList.add('pc--error')
			}
		} else if (error.value == "street") {
			if (error.parent == "shippingAddress") {
				document.querySelector('.pc__account-info__shipping-street').classList.add('pc--error')
				document.querySelector('.pc__account-info__shipping-number').classList.add('pc--error')
			} else {
				document.querySelector('.pc__account-info__billing-street').classList.add('pc--error')
				document.querySelector('.pc__account-info__billing-number').classList.add('pc--error')
			}
		}

	});
	if (errorValues.length == 0) {
		return true;
	} else {
		return false;
	}


}

function succesfullOrder() {
	var url = new URL(window.location.href);
	return Boolean(url.searchParams.get("successfull_order"));
}
export {
	pcCheckout,
	pcShipping,
	succesfullOrder
}