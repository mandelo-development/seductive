import LocomotiveScroll from '../../../config/node_modules/locomotive-scroll';

export const scrollCont = new LocomotiveScroll({
    el: document.querySelector('#js-scroll'),
    smooth: true,
    smoothMobile: false,
    useKeyboard: false,
    lerp: 0.1,
    scrollFromAnywhere: true,
});