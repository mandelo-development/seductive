import '../../config/node_modules/regenerator-runtime/runtime';
import {platecommerce} from './scripts/ecommerce/platecommerce';
import {contentAnimationIn} from './scripts/animationTitle';
import {gallery} from './scripts/gallery';
import './scripts/functions';
import './scripts/updatecss';
import {navigation} from './scripts/navigation';
import './scripts/swiper';
import './scripts/form';
import {filter} from './scripts/filter'; 
import {general} from './scripts/general';
import '../child_theme/src/scripts/script';
import {lazyload} from './scripts/lazyload';
import { shopEvents } from './scripts/ecommerce/shopEvents';
import { authentication } from './scripts/authentication/authentication';
import './styles/style';

if (document.getElementsByTagName('body')[0].dataset['shop'] == 'true'){
    platecommerce();
} else {
    console.log('no-shop');
};

contentAnimationIn();
navigation();
filter();
general();
lazyload(document);

if (document.getElementsByTagName('body')[0].dataset['shop'] == 'true'){
    shopEvents();
};

authentication();
gallery();